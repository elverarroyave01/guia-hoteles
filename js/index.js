$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});
$(function () {
    $('[data-toggle="popover"]').popover();
});
$('.carousel').carousel({
    interval: 2000
});

$('#staticBackdrop').on('show.bs.modal', function (e) {
    console.log('Se comneso a abrir el modal');
    $('#jquery-modal').removeClass('btn-outline-success');
    $('#jquery-modal').addClass('btn-outline-primary');
    $('#jquery-modal').prop('disabled', true);
});
$('#staticBackdrop').on('shown.bs.modal', function (e) {
    $('#jquery-modal').prop('disabled', false);
    console.log('Se termino de abrir');
});
$('#staticBackdrop').on('hide.bs.modal', function (e) {
    console.log('Se comenzo a ocultar');
});
$('#staticBackdrop').on('hidden.bs.modal', function (e) {
    console.log('Se termino de ocultar');
});